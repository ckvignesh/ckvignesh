### Hi there 👋 I'm Vignesh.

- 🌱 I’m currently learning Frontend Web development and building projects.
- 👯 Looking to collaborate on Projects related to frontend, preferably Web3 and blockchains.
- 📫 You can have a look at my ongoing projects, [here](https://github.com/ckvignesh?tab=repositories).


### Connect with me here:

<p align="center">
	<a href="https://www.linkedin.com/in/ck-vignesh-/">
		<img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" />
	</a>
	<a href="https://twitter.com/vigneshCodes">
		<img src="https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white" />
	</a>
<!-- 	<a href="https://www.instagram.com/userName/">
		<img src="https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white" />
	</a> -->
	<a href="https://ckvignesh.hashnode.dev/">
		<img src="https://img.shields.io/badge/Hashnode-2962FF?style=for-the-badge&logo=hashnode&logoColor=white" />
	</a>
	<a href="https://dev.to/ckvignesh">
		<img src="https://img.shields.io/badge/dev.to-0A0A0A?style=for-the-badge&logo=devdotto&logoColor=white" />
	</a>
<!--   <a href="https://ckvignesh.github.io/">
		<img src="https://img.shields.io/badge/portfolio-1AA260?style=for-the-badge&logo=About.me&logoColor=white" />
	</a> -->
  <a href="mailto:writer.vignesh.ck@gmail.com">
		<img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" />
	</a>
</p>

### Love to work with:
<p align="center">
  <img src="https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white" height="25">
  <img src="https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white" height="25">
  <img src="https://img.shields.io/badge/javascript-F7DF1E.svg?&style=for-the-badge&logo=javascript&logoColor=white" height="25"/>
  <img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB" height="25">
  <img src="https://img.shields.io/badge/VS%20Code-007ACC.svg?&style=for-the-badge&logo=visual-studio-code&logoColor=white" height="25"/>
  <img src="https://img.shields.io/badge/Git-F05032?style=for-the-badge&logo=git&logoColor=white" height="25">
  <img src="https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white" height="25">
  <img src="https://img.shields.io/badge/Markdown-000000?style=for-the-badge&logo=markdown&logoColor=white" height="25">
  <img src="https://img.shields.io/badge/Heroku-430098?style=for-the-badge&logo=heroku&logoColor=white" height="25">
  <img src="https://img.shields.io/badge/Amazon_AWS-232F3E?style=for-the-badge&logo=amazon-aws&logoColor=white" height="25">
  <img src="https://img.shields.io/badge/C-00599C?style=for-the-badge&logo=c&logoColor=white" height="25">
</p>

***
<p align='center'>
  <a href="https://github.com/ckvignesh/github-readme-stats"><img src="https://github-readme-stats.vercel.app/api?username=ckvignesh&show_icons=true&include_all_commits=true&theme=tokyonight" alt="Vignesh's github stats" />
  </a>
  <br>
  <a href="https://github.com/ckvignesh/github-readme-stats"><img src="https://github-readme-streak-stats.herokuapp.com/?user=ckvignesh&theme=tokyonight" />
  </a>
</p>
<br>
<p align='center'>
  <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=ckvignesh&layout=compact&theme=tokyonight" />
</p>


***
### :zap: Updates:

<!--START_SECTION:activity-->
1. 🎉 Merged PR [#3](https://github.com/ckvignesh/resume/pull/3) in [ckvignesh/resume](https://github.com/ckvignesh/resume)
2. ❗️ Closed issue [#1](https://github.com/ckvignesh/resume/issues/1) in [ckvignesh/resume](https://github.com/ckvignesh/resume)
3. 💪 Opened PR [#3](https://github.com/ckvignesh/resume/pull/3) in [ckvignesh/resume](https://github.com/ckvignesh/resume)
4. 🗣 Commented on [#4](https://github.com/ckvignesh/ckvignesh.github.io/issues/4) in [ckvignesh/ckvignesh.github.io](https://github.com/ckvignesh/ckvignesh.github.io)
5. ❗️ Closed issue [#3](https://github.com/ckvignesh/ckvignesh.github.io/issues/3) in [ckvignesh/ckvignesh.github.io](https://github.com/ckvignesh/ckvignesh.github.io)
6. ❗️ Opened issue [#10](https://github.com/ckvignesh/ckvignesh/issues/10) in [ckvignesh/ckvignesh](https://github.com/ckvignesh/ckvignesh)
7. ❗️ Opened issue [#9](https://github.com/ckvignesh/ckvignesh/issues/9) in [ckvignesh/ckvignesh](https://github.com/ckvignesh/ckvignesh)
8. ❗️ Opened issue [#8](https://github.com/ckvignesh/ckvignesh/issues/8) in [ckvignesh/ckvignesh](https://github.com/ckvignesh/ckvignesh)
9. ❗️ Opened issue [#7](https://github.com/ckvignesh/ckvignesh/issues/7) in [ckvignesh/ckvignesh](https://github.com/ckvignesh/ckvignesh)
10. ❗️ Opened issue [#6](https://github.com/ckvignesh/ckvignesh/issues/6) in [ckvignesh/ckvignesh](https://github.com/ckvignesh/ckvignesh)
<!--END_SECTION:activity-->

<!-- *** -->

<!-- ### :zap: Weekly Report: -->

<!--START_SECTION:waka-->



<!--END_SECTION:waka-->

***
<p align='center'>
  <img src="https://img.shields.io/badge/Made%20with-Markdown-1f425f.svg" height="25">
  <img src="https://img.shields.io/badge/Built%20with-VSCode-1f425f.svg" height="25">
</p>
<p align='center'><img src='https://visitor-badge.laobi.icu/badge?page_id=ckvignesh'></p>
